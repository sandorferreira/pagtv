//
//  Movie.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

class Movie: Codable {
    let title: String
    let genres: [Int]
    let overview: String
    let releaseDate: String
    let posterPath: String
    let voteAverage: Double
    let vote_count: Int
    
    private enum CodingKeys: String, CodingKey {
        case title
        case genres = "genre_ids"
        case overview
        case releaseDate = "release_date"
        case posterPath = "poster_path"
        case voteAverage = "vote_average"
        case vote_count
    }
    
    init(title: String, genres: [Int], overview: String, releaseDate: String, posterPath: String, voteAverage: Double, vote_count: Int) {
        self.title = title
        self.genres = genres
        self.overview = overview
        self.releaseDate = releaseDate
        self.posterPath = posterPath
        self.voteAverage = voteAverage
        self.vote_count = vote_count
    }
}
