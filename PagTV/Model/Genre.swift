//
//  Genre.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

struct Genre: Codable {
    var id: Int
    var name: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}
