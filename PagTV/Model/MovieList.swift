//
//  MovieList.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

class MovieList: Codable {
    let mainMovies: [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case mainMovies = "results"
    }
}

class GenreList: Codable {
    let genres: [Genre]?
    
    private enum CodingKeys: String, CodingKey {
        case genres
    }
}
