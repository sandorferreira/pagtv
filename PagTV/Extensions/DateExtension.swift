//
//  DateExtension.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

extension Date {
    
    func stringFromDate() -> String {
        let local = Locale(identifier: "en_US_POSIX")
        let formatter = DateFormatter()
        formatter.locale = local
        formatter.dateFormat = "yyyy-mm-dd"
        
        return formatter.string(from: self)
    }
    
    func friendlyString() -> String {
        let day = Calendar.current.component(.day, from: self)
        let strDay = day < 10 ? "0\(day)" : "\(day)"
        let month = Calendar.current.component(.month, from: self)
        let strMonth = month < 10 ? "0\(month)" : "\(month)"
        let year = Calendar.current.component(.year, from: self)
        
        return "\(strDay)/\(strMonth)/\(year)"
    }
    
}
