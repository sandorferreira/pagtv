//
//  MovieCollectionViewCell.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 03/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class FavMovieCollectionViewCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Movie"
        return label
    }()
    
    let posterImage: UIImageView = {
        let poster = UIImageView()
        poster.contentMode = .scaleAspectFit
        poster.image = UIImage(named: "poster_ex")
        return poster
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("not implemented")
    }
    
    private func setupViews() {
        //addSubview(titleLabel)
        addSubview(posterImage)
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        posterImage.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        posterImage.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        posterImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
    }
}
