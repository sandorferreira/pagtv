//
//  ViewController.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import CoreData

class PagTVViewController: UITableViewController {
    
    let cellId = "MovieCell"
    var moviesList: MovieListViewModel?
    var moviesFromGenre: [String: [MovieViewModel]]?
    var genres: [Genre]?
    var favorites = [MovieViewModel]()
    let refresh = UIRefreshControl()
    let activityIndicator: UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: .whiteLarge)
        act.backgroundColor = .black
        act.translatesAutoresizingMaskIntoConstraints = false
        act.startAnimating()
        return act
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupActivityView()
        setupViews()
        requestCoreData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestData()
    }
    
    @objc private func refreshData() {
        requestData()
        requestCoreData()
    }
    
    private func setupViews() {
        tableView.register(FavMovieTVCell.self, forCellReuseIdentifier: cellId)
        tableView.backgroundColor = .black
        tableView.separatorStyle = .none
        tableView.rowHeight = 170
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refresh
        } else {
            tableView.addSubview(refresh)
        }
        self.refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.navigationItem.title = "pag!TV"
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)]
    }
    
    private func setupActivityView() {
        guard let window = UIApplication.shared.keyWindow else { return }
        
        window.addSubview(activityIndicator)
        window.bringSubviewToFront(activityIndicator)
        
        activityIndicator.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
        activityIndicator.heightAnchor.constraint(equalToConstant: window.bounds.height).isActive = true
        activityIndicator.widthAnchor.constraint(equalToConstant: window.bounds.width).isActive = true
    }
    
    private func requestCoreData() {
        self.favorites = [MovieViewModel]()
        self.favorites = CoreDataUtils.fetchFavoriteMovies()
        self.tableView.reloadData()
    }

    private func requestData() {
        self.moviesFromGenre = [String: [MovieViewModel]]()
        if Reachability.isConnectedToNetwork(){
            PagTVAPI.requestMovies { (movieList, success) in
                if success {
                    self.moviesList = MovieListViewModel(movieList: movieList!)
                    PagTVAPI.getGenresFromId { (genres, success) in
                        if success {
                            self.genres = genres
                            self.moviesFromGenre = self.moviesList?.moviesFromGenre(genres!)
                            self.refresh.endRefreshing()
                            self.activityIndicator.stopAnimating()
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    self.requestCoreData()
                    self.activityIndicator.stopAnimating()
                }
            }
        }else{
            self.requestCoreData()
            self.refresh.endRefreshing()
            self.activityIndicator.stopAnimating()
        }
        
    }

}

extension PagTVViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        let moviesCount = self.moviesFromGenre?.count ?? 0
        let favoriteCount = self.favorites.count != 0 ? 1 : 0
        return moviesCount + favoriteCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        returnedView.backgroundColor = .black
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width, height: 40))
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .white
        if favorites.count != 0 {
            if section == 0 {
                label.text = "Favoritos"
                label.textColor = .black
                returnedView.backgroundColor = UIColor.yellow
            } else {
                label.text = Array((self.moviesFromGenre?.keys)!)[section-1]
            }
        } else {
            label.text = Array((self.moviesFromGenre?.keys)!)[section]
        }
        returnedView.addSubview(label)
        
        
        return returnedView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! FavMovieTVCell
        if favorites.count != 0 {
            if indexPath.section == 0 {
                cell.collectionMovieView.movieListViewModel = self.favorites
            } else {
                let auxgenre = Array((self.moviesFromGenre?.keys)!)[indexPath.section-1]
                cell.collectionMovieView.movieListViewModel = self.moviesFromGenre?[auxgenre]
            }
        } else {
            let auxgenre = Array((self.moviesFromGenre?.keys)!)[indexPath.section]
            cell.collectionMovieView.movieListViewModel = self.moviesFromGenre?[auxgenre]
        }
        cell.collectionMovieView.viewController = self
        cell.collectionMovieView.collectionView.reloadData()
        
        
        return cell
    }
}
