//
//  DetailViewController.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 09/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var posterIV: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var overviewTV: UITextView!
    
    // Data
    var movie: MovieViewModel?
    var genres: [Genre]?
    var viewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initGUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBar.barStyle = .default
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initGUI() {
        self.titleLabel.text = self.movie?.title
        self.voteCountLabel.text = "\(self.movie!.voteCount)"
        self.voteAverageLabel.text = "\(self.movie!.voteAverage)"
        self.overviewTV.text = !(self.movie?.overview.isEmpty)! ? self.movie?.overview : "Sinopse não disponível"
        PagTVAPI.getPosterOfMovie(posterPath: (self.movie?.poster)!) { (success, image) in
            if success {
                self.posterIV.image = image
            }
        }
        initGenresLabel()
        initYearLabel()
        initFavoriteButton()
    }
    
    private func initGenresLabel() {
        PagTVAPI.getGenresFromIds(genreIds: self.movie!.genreIds) { (genres, success) in
            if success {
                var string = genres!.first!.name //self.genres!.first!.name
                for genre in genres! {
                    if genre.name != string {
                        string = string + " | \(genre.name)"
                    }
                }
                self.genresLabel.text = string
            }
        }
        
    }
    
    private func initYearLabel() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        guard let date = dateFormatter.date(from: self.movie!.stringDate) else {return}
        self.yearLabel.text = date.friendlyString()
    }
    
    private func initFavoriteButton() {
        let isMovieFavorite = CoreDataUtils.isMovieFavorite(movie: self.movie)
        if isMovieFavorite {
            updateButton(config: isMovieFavorite)
        } else {
            updateButton(config: isMovieFavorite)
        }
    }
    
    // saving to favorites
    
    @IBAction func save(_ sender: Any) {
        let isMovieFavorite = CoreDataUtils.isMovieFavorite(movie: self.movie)
        if isMovieFavorite {
            updateButton(config: !isMovieFavorite)
            print("Desfavoritar")
            CoreDataUtils.removeFromCore(movie: self.movie)
        } else {
            updateButton(config: !isMovieFavorite)
            print("Favoritar!")
            CoreDataUtils.saveToCore(movie: self.movie)
        }
    }
    
    func updateButton(config: Bool) {
        if config {
            self.favoriteButton.setImage(UIImage(named: "ic_favorite_24px"), for: .normal)
            self.favoriteButton.setImage(UIImage(named: "ic_favorite_border_24px"), for: .highlighted)
        } else {
            self.favoriteButton.setImage(UIImage(named: "ic_favorite_border_24px"), for: .normal)
            self.favoriteButton.setImage(UIImage(named: "ic_favorite_24px"), for: .highlighted)
        }
    }
    
}
