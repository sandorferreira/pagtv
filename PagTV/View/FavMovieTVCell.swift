//
//  FavMovieTVCell.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 03/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class FavMovieTVCell: UITableViewCell {
    lazy var collectionMovieView: MovieCollectionView = {
        return MovieCollectionView()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(collectionMovieView.collectionView)
        collectionMovieView.collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionMovieView.collectionView.topAnchor.constraint(equalTo: topAnchor).isActive
            = true
        collectionMovieView.collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionMovieView.collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionMovieView.collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    }
}
