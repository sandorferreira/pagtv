//
//  MovieCollectionView.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 03/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class MovieCollectionView: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let collectionId = "collectionId"
    var movieListViewModel: [MovieViewModel]?
    var genre: Genre?
    var viewController: UIViewController?
    
    lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let cView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        cView.backgroundColor = .black
        
        cView.delegate = self
        cView.dataSource = self
        
        return cView
    }()
    
    override init() {
        super.init()
        collectionView.register(FavMovieCollectionViewCell.self, forCellWithReuseIdentifier: collectionId)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieListViewModel?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! FavMovieCollectionViewCell
        if let posterPath = self.movieListViewModel?[indexPath.row].poster {
            PagTVAPI.getPosterOfMovie(posterPath: posterPath) { (success, image) in
                if success {
                    cell.posterImage.image = image
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Details", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Detail") as! DetailViewController
        controller.movie = self.movieListViewModel![indexPath.row]
        controller.viewController = self.viewController
        self.viewController!.present(controller, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width * 0.3
        return CGSize(width: width, height: width * (4 / 3))
    }
}

