//
//  MovieListViewModel.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 02/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

class MovieListViewModel {
    private var movieList: MovieList
    private var moviesFromGenre = [String: [MovieViewModel]]()
    
    init(movieList: MovieList) {
        self.movieList = movieList
    }
    
    public func moviesFromGenre(_ genres: [Genre]) -> [String: [MovieViewModel]] {
        for genre in genres {
            var auxMoviesVM = [MovieViewModel]()
            for movie in self.movieList.mainMovies! {
                if movie.genres.contains(genre.id) && !auxMoviesVM.contains(where: {$0.title == movie.title}){
                    auxMoviesVM.append(MovieViewModel(movie: movie))
                }
            }
            if auxMoviesVM.count != 0 {
                self.moviesFromGenre[genre.name] = auxMoviesVM
            }
        }
        return self.moviesFromGenre
    }
}
