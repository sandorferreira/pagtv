//
//  MovieViewModel.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class MovieViewModel {
    private let movie: Movie
    
    public init(movie: Movie) {
        self.movie = movie
    }
    
    public var title: String {
        return movie.title
    }

    public var poster: String? {
        return self.movie.posterPath
    }
    
    public var overview: String {
        return movie.overview
    }
    
    public var stringDate: String {
        return movie.releaseDate
    }
    
    public var voteCount: Int {
        return movie.vote_count
    }
    
    public var voteAverage: Double {
        return movie.voteAverage
    }
    
    public var genreIds: [Int] {
        return movie.genres
    }
    
}
