//
//  CoreDataUtils.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 11/04/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import CoreData
import UIKit

class CoreDataUtils {
    
     static func fetchFavoriteMovies() -> [MovieViewModel] {
        var favMovies = [MovieViewModel]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return [MovieViewModel]()}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MMovie")
        
        do {
            let favoriteMovies = try managedContext.fetch(fetchRequest)
            for favorite in favoriteMovies {
                let title = favorite.value(forKey: "title") as! String
                let genres = favorite.value(forKey: "genres") as! [Int]
                let overview = favorite.value(forKey: "overview") as! String
                let posterPath = favorite.value(forKey: "posterPath") as! String
                let releaseDate = favorite.value(forKey: "releaseDate") as! String
                let voteCount = favorite.value(forKey: "vote_count") as! Int
                let voteAverage = favorite.value(forKey: "voteAverage") as! Double
                let movie = Movie(title: title, genres: genres, overview: overview, releaseDate: releaseDate, posterPath: posterPath, voteAverage: voteAverage, vote_count: voteCount)
                let movieViewModel = MovieViewModel(movie: movie)
                if !favMovies.contains(where: {$0.title == title}) {
                    favMovies.append(movieViewModel)
                }
            }
            //self.tableView.reloadData()
        } catch let error as NSError {
            print(error)
        }
        return favMovies
    }
    
    static func saveToCore(movie: MovieViewModel?) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "MMovie", in: managedContext)!
        
        let mmovie = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // 3
        mmovie.setValue(movie?.title, forKeyPath: "title")
        mmovie.setValue(movie?.voteCount, forKey: "vote_count")
        mmovie.setValue(movie?.voteAverage, forKey: "voteAverage")
        mmovie.setValue(movie?.stringDate, forKey: "releaseDate")
        mmovie.setValue(movie?.poster, forKey: "posterPath")
        mmovie.setValue(movie?.overview, forKey: "overview")
        mmovie.setValue(movie?.genreIds, forKey: "genres")
        
        // 4
        do {
            try managedContext.save()
            //people.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    static func removeFromCore(movie: MovieViewModel?) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MMovie")
        
        do {
            let favoriteMovies = try managedContext.fetch(fetchRequest)
            for favorite in favoriteMovies {
                if favorite.value(forKey: "title") as? String == movie?.title {
                    managedContext.delete(favorite)
                    do {
                        try managedContext.save()
                    } catch let error {
                        print(error)
                    }
                }
            }
        } catch let error {
            print(error)
        }
    }
    
    static func isMovieFavorite(movie: MovieViewModel?) -> Bool {
        var isMovieFavorite = false
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return false}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MMovie")
        
        do {
            let favoriteMovies = try managedContext.fetch(fetchRequest)
            for favorite in favoriteMovies {
                if favorite.value(forKey: "title") as? String == movie?.title {
                    isMovieFavorite = true
                }
            }
        } catch let error {
            print(error)
        }
        return isMovieFavorite
    }
}
