//
//  PagTVAPI.swift
//  PagTV
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import Alamofire

protocol PagTVMoviesDelegate {
    func response(status: Int, moviesList: MovieList)
}

public class PagTVAPI {
    
    
    typealias completionBlock = (_ success: Bool, _ data: NSDictionary) -> Void
    var delegate: PagTVMoviesDelegate! // garantindo
    
    func requestGenres(completed: @escaping completionBlock) {
        //let stringURL =
    }
    
    static func requestMovies(handler: @escaping ((MovieList?, Bool) -> Void)) {
        let url = "https://api.themoviedb.org/3/movie/upcoming"
        let api_key = "1f54bd990f1cdfb230adb312546d765d"
        let language = "pt-BR"
        let page = "1"
        let baseURL = "\(url)?api_key=\(api_key)&language=\(language)&page=\(page)"
        
        request(baseURL).responseData { (response) in
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let movies = try decoder.decode(MovieList.self, from: data)
                    handler(movies, true)
                } catch let erro {
                    handler(nil, false)
                    print(erro)
                }
            } else {
                handler(nil,false)
            }
        }
        
    }
    
    static func getMoviesFromGenre(genre: Genre, handler: @escaping (([MovieViewModel])?,Bool) -> Void) {
        self.requestMovies { (movieList, success) in
            if success {
                var auxMovies = [MovieViewModel]()
                for movie in movieList!.mainMovies! {
                    if movie.genres.contains(genre.id) && !auxMovies.contains(where: {$0.title == movie.title}){
                        auxMovies.append(MovieViewModel(movie: movie))
                    }
                }
                handler(auxMovies, true)
            } else {
                handler(nil, false)
            }
        }
    }
    
    static func getGenresFromIds(genreIds: [Int], handler: @escaping (([Genre]?, Bool) -> Void)) {
        let url = "https://api.themoviedb.org/3/genre/movie/list"
        let api_key = "1f54bd990f1cdfb230adb312546d765d"
        let language = "pt-BR"
        let baseURL = url + "?api_key=\(api_key)&language=\(language)"
        
        request(baseURL).responseData { (response) in
            if let data = response.data {
                do {
                    var auxGenres = [Genre]()
                    let decoder = JSONDecoder()
                    let genres = try decoder.decode(GenreList.self, from: data)
                    for genre in genres.genres! {
                        if genreIds.contains(genre.id) {
                            auxGenres.append(genre)
                        }
                    }
                    handler(auxGenres,true)
                } catch let erro {
                    handler(nil, false)
                    print(erro.localizedDescription)
                }
            }
        }
    }
    
    static func getGenresFromId(handler: @escaping (([Genre]?, Bool) -> Void)) {
        let url = "https://api.themoviedb.org/3/genre/movie/list"
        let api_key = "1f54bd990f1cdfb230adb312546d765d"
        let language = "pt-BR"
        let baseURL = url + "?api_key=\(api_key)&language=\(language)"
        
        request(baseURL).responseData { (response) in
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let genres = try decoder.decode(GenreList.self, from: data)
                    handler(genres.genres,true)
                } catch let erro {
                    handler(nil, false)
                    print(erro.localizedDescription)
                }
            }
        }
    }
    
    static public func getPosterOfMovie(posterPath: String, handler: @escaping ((Bool,UIImage) -> Void)) {
        let baseURL = "https://image.tmdb.org/t/p/w185"
        let imageURL = baseURL + posterPath
        guard let url = URL(string: imageURL) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { handler(false,UIImage()); return }
            DispatchQueue.main.async() {
                handler(true,image)
            }
            }.resume()
        handler(false,UIImage())
    }
    
}
