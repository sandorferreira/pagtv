//
//  PagTVTests.swift
//  PagTVTests
//
//  Created by Sandor ferreira da silva on 28/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import XCTest
@testable import PagTV

class PagTVTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMovieViewModel() {
        let movie = Movie(title: "title", genres: [0,1], overview: "overview", releaseDate: "dd/mm/yyyy", posterPath: "posterPath", voteAverage: 0.0, vote_count: 7)
        let movieViewModel = MovieViewModel(movie: movie)
        
        XCTAssertEqual(movie.title, movieViewModel.title)
        XCTAssertEqual(movie.genres, movieViewModel.genreIds)
        XCTAssertEqual(movie.overview, movieViewModel.overview)
        XCTAssertEqual(movie.releaseDate, movieViewModel.stringDate)
        XCTAssertEqual(movie.posterPath, movieViewModel.poster)
        XCTAssertEqual(movie.voteAverage, movieViewModel.voteAverage)
        XCTAssertEqual(movie.vote_count, movieViewModel.voteCount)
        
    }
    
    func testConnected() {
        let isReached = Reachability.isConnectedToNetwork()
        // checando conexao
        XCTAssertTrue(isReached)
    }
    
    func testConsistenciaQualquerMovie() {
        let movie = Movie(title: "title", genres: [0,1], overview: "overview", releaseDate: "dd/mm/yyyy", posterPath: "posterPath", voteAverage: 0.0, vote_count: 7)
        let movieViewModel = MovieViewModel(movie: movie)
        let isFavorite = CoreDataUtils.isMovieFavorite(movie: movieViewModel)
        
        // dá true para qualquer movie??
        XCTAssertFalse(isFavorite)
    }

//    func testConsistenciaFavoriteMovie() {
//        // todos estão como favoritos??
//        for movieViewModel in CoreDataUtils.fetchFavoriteMovies() {
//            XCTAssertTrue(CoreDataUtils.isMovieFavorite(movie: movieViewModel))
//        }
//    }

    
    // APENAS PARA INTERNET DESCONECTADA
    
//    func testNotConnected() {
//        let isReached = Reachability.isConnectedToNetwork()
//        // checando conexao
//        XCTAssertFalse(isReached)
//    }
    
    
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
